#include<stdio.h>
#include<time.h>

typedef struct OWN
{
	int 	    data;
	struct OWN *next;
} OWN;

typedef struct LOWL
{
	OWN *beg;
	OWN *cur;
} LOWL;

unsigned int lowl_get_size(LOWL *list)
{
	OWN *help;
	unsigned int count=0;
	
	help=list->beg;
	
	if(help)
	{
		while(help!=NULL)
		{
			count++;
			help=help->next;
		}
	}
	
	else count=0;
	
	return count;	
}

LOWL *create_random (unsigned int size)
{
	LOWL *list;
	unsigned int i;
	
	srand(time(NULL));
	
	list=malloc(sizeof(LOWL));
	
	if(list==NULL)
		return 0;
					
	for(i=0;i<(size);i++)
	{
		if (i==0)
		{
			list->beg=malloc(sizeof(OWN));
			list->cur=list->beg;
			list->cur->data=rand()%100;
		}
		
		if(i<(size-1))
		{
			list->cur->next=malloc(sizeof(OWN));
			list->cur=list->cur->next;
			list->cur->data=rand()%100;
		}
		
	
		if (i==size-1)
			list->cur->next=NULL;
		
	}
	
	list->cur=list->beg;
	
	if (list->beg==NULL)
		return 0;
		
	return list;		
}

void lowl_print(LOWL *list)
{
	OWN *help;
	unsigned int i=1;
	
	help=list->beg;
	
	if(help)
	{
		while(help!=NULL)
		{
			if(help==list->cur)
				printf("cur: ");
			
			printf("%d: %d\n",i,help->data);
			help=help->next;
			i++;
		}
	}
}

int main()
{
	LOWL *list;
	
	list=create_random(5);
	
	lowl_print(list);
	
	printf("pocet zaznamov je: %d\n",lowl_get_size(list));
	
	
}
