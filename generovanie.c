#include<stdio.h>
#include<time.h>

typedef struct OWN
{
	int 	    data;
	struct OWN *next;
} OWN;

typedef struct LOWL
{
	OWN *beg;
	OWN *cur;
} LOWL;

unsigned int lowl_get_size(LOWL *list)
{
	OWN *help;
	unsigned int count=0;
	
	help=list->beg;
	
	if(help)
	{
		while(help->next!=NULL)
		{
			count++;
			help=help->next;
		}
	}
	
	else count=0;
	
	return count;	
}

LOWL *create_random (unsigned int size)
{
	LOWL *list;
	unsigned int i;
	
	srand(time(NULL));
	
	list=malloc(sizeof(LOWL));
	
	if(list==NULL)
		return 0;
					
	for(i=0;i<size;i++)
	{
		if (i==0)
		{
			list->beg=malloc(sizeof(OWN));
			list->cur=list->beg;
		}
		
		list->cur->data=rand()%100;
		list->cur->next=malloc(sizeof(OWN));
		list->cur=list->cur->next;

		if (i==size-1)
			list->cur->next=NULL;

	}
	
	list->cur=list->beg;
	
	if (list->beg==NULL)
		return 0;
		
	return list;		
}

int main()
{
	
}
