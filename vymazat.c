#include<stdio.h>
#include<time.h>

typedef struct OWN
{
	int 	    data;
	struct OWN *next;
} OWN;

typedef struct LOWL
{
	OWN *beg;
	OWN *cur;
} LOWL;

unsigned int lowl_get_size(LOWL *list)
{
	OWN *help;
	unsigned int count=0;
	
	help=list->beg;
	
	if(help)
	{
		while(help!=NULL)
		{
			count++;
			help=help->next;
		}
	}
	
	else count=0;
	
	return count;	
}

LOWL *lowl_create_empty(void)
{
	LOWL *list;
	
	list=malloc(sizeof(LOWL));
	
	if(list==NULL)
		return 0;
	
	else
	{
		list->beg=NULL;
		list->cur=NULL;
		list->cur->next=NULL;
	}
	
	return list;
	
}

void lowl_destroy(LOWL *list)
{
	free(list);
}

LOWL *create_random (unsigned int size)
{
	LOWL *list;
	unsigned int i;
	
	srand(time(NULL));
	
	list=malloc(sizeof(LOWL)); 
	
	if(list==NULL)
		return 0;
					
	for(i=0;i<(size);i++)
	{
		if (i==0)
		{
			list->beg=malloc(sizeof(OWN));
			list->cur=list->beg;
			list->cur->data=rand()%100;
		}
		
		if(i<(size-1))
		{
			list->cur->next=malloc(sizeof(OWN));
			list->cur=list->cur->next;
			list->cur->data=rand()%100;
		}
		
	
		if (i==size-1)
			list->cur->next=NULL;
		
	}
	
	list->cur=list->beg;
	
	if (list->beg==NULL)
		return 0;
		
	return list;		
}

void lowl_print(LOWL *list)
{
	OWN *help;
	unsigned int i=1;
	
	help=list->beg;
	
	if(help)
	{
		while(help!=NULL)
		{
			if(help==list->cur)
				printf("cur: ");
			
			printf("%d: %d\n",i,help->data);
			help=help->next;
			i++;
		}
		
		printf("\n");
	}
}

char lowl_cur_step_right(LOWL *list)
{
	
	if(list->cur->next==NULL)
		return 0;
		
	else 
	{
		list->cur=list->cur->next;
		return 1;	
	}
	
}

char lowl_cur_step_left(LOWL *list)
{
	OWN *help;
			
	if(list->cur==list->beg)
		return 0;
				
	else 
	{
		help=list->cur;
		list->cur=list->beg;
		
		while(list->cur->next!=help)
			list->cur=list->cur->next;
						
		return 1;
	}
}

OWN *lowl_insert_left(LOWL* list, float val)
{
	OWN *help,*new;
	
	
	if(list->cur==list->beg)
		return 0;
		
	else 
	{
		new=malloc(sizeof(OWN));
		new->data=val;
		
		help=list->cur;
		list->cur=list->beg;
		
		while(list->cur->next!=help)
			list->cur=list->cur->next;
			
		new->next=list->cur->next;
		list->cur->next=new;
		
		list->cur=new->next;
		
		return new;
	}	
}

OWN *lowl_insert_right(LOWL* list, float val)
{
	OWN *new;
	
	if(list->cur->next==NULL)
		return 0;
		
	else 
	{
		new=malloc(sizeof(OWN));
		new->data=val;
		
		new->next=list->cur->next;
		list->cur->next=new;
		
		return new;
		
	}	
}

float lowl_delete(LOWL* list)
{
	float val;
	OWN *help,*helpn;

	val=list->cur->data;
		
	if(list->cur==list->beg)
	{
	
		list->beg=list->cur->next;
		
		free(list->cur);
		
		list->cur=list->beg;
		
		return val;
	}
	
	else 
		if(list->cur->next==NULL)	
		{
			
		
			help=list->cur;
			list->cur=list->beg;
		
			while(list->cur->next!=help)
				list->cur=list->cur->next;
		
			
			free(list->cur->next);
			
			list->cur->next=NULL;
			
			return val;
		}
	
		else 
			{
				
				help=list->cur;
				list->cur=list->beg;
				
				while(list->cur->next!=help)
					list->cur=list->cur->next;
				
				helpn=help->next;
				
				free(list->cur->next);
				
				list->cur->next=helpn;
				list->cur=helpn;
						
				return val;
			}
}


int main()
{
	LOWL *list;
	
	list=create_random(5);
	
	lowl_print(list);
	
	printf("pocet zaznamov je: %d\n\n",lowl_get_size(list));
	
	printf("posun doprava\n");
	lowl_cur_step_right(list);
	lowl_print(list);
	
	
	printf("pridanie vlavo\n");
	lowl_insert_left(list,111);
	lowl_print(list);
	
	printf("pridanie vpravo\n");
	lowl_insert_right(list,222);
	lowl_print(list);
	
	printf("posun dolava\n");
	lowl_cur_step_left(list);
	lowl_print(list);
	
	printf("vymazanie\n");
	lowl_delete(list);
	lowl_print(list);
	
}
